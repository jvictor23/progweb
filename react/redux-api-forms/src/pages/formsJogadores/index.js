import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { gravarJogador, buscarJogador, jogadorLimpo, atualizarJogador } from '../../redux/actions/jogadoresActions';
import SideBar from '../sideBar';
import TopBar from '../topBar';
import { connect } from 'react-redux'

class FormJogadores extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            titulo : "",
        };
      }

    componentDidMount(){

        let tipo = this.props.match.path;
        if(tipo.includes("edit")){
            this.setState({titulo:"Editar Jogador"})
            this.props.dispatch(buscarJogador(this.props.match.params.id))
        }else{
            this.setState({titulo:"Novo Jogador"})
            this.props.dispatch(jogadorLimpo());
            console.log("Novo Jogador");
        }

    }

    submit (props, data, gravarJogador) {
        if(this.state.titulo.includes("Editar")){
            atualizarJogador(data, props)
        }else{
            gravarJogador(data, props)
        }
    }



    render() {
        
        
        const {handleSubmit} = this.props;

        return (
            <div className="container">
                <TopBar />
                <div className="row">
                    <div className="col-3">
                        <SideBar />
                    </div>
                    <div className="col-9">
                        <h1> {this.state.titulo} </h1>
                        <form onSubmit={handleSubmit((fields) => this.submit(this.props, fields, gravarJogador))}>
                            <div className="form-group">
                                <label>Nome</label>
                                <Field component="input" name="name" type="text" className="form-control" defaultValue={this.state.nome}/>
                            </div>
                            <div className="form-group">
                                <label>Nacionalidade</label>
                                <Field component="input" name="country" type="text" className="form-control"/>
                            </div>
                            <div className="form-group">
                                <label>Biografia</label>
                                <Field component="textarea" name="bio" className="form-control"/>
                            </div>

                            <div className="form-group">
                                <label>Ano de nascimento</label>
                            <Field component="input" type="number" name="nascimento" className="form-control" />
                            </div>

                           

                            <button type="submit" className="btn btn-primary">
                                Gravar
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}


FormJogadores = reduxForm({
    form: 'jogadores',
    enableReinitialize: true
})(FormJogadores)

FormJogadores = connect(state => ({
    initialValues: state.jogador.jogador  
  }), { load: buscarJogador })(FormJogadores);
  

export default FormJogadores    